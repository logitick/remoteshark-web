# RemoteShark
![Builds Status](https://codeship.com/projects/ab8dfa00-192e-0133-9cc3-06b4654508b3/status)
## Milestones
###0.4.0
* Complete Web site
* Home
* FAQ 
* Freelancer
* Employer
* Contact Us   
### 0.5.0
* Authentication and sign up
* Freelancer Profile Creation and Update
* Marketplace (listing only)
* Private listing of user's own posts
###0.9.0.ALPHA
* Employer Profile creation and update
* Employer post to job board
* Job Board
* Messaging
### 1.0.0
* User Rating
* Administer Ratings
* Featured Job Post
* Administer Users
* Administer Job Posts
