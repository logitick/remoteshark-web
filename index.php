<?php

$loader = require_once __DIR__.'/vendor/autoload.php';
$loader->add('Alliance\\', __DIR__.'/src');


error_reporting(E_ALL);
$app = new Silex\Application();
$app['debug'] = true;

$app['resolver'] = function() use ($app) {
	return new RemoteShark\Core\Controller\ControllerResolver($app, $app['logger']);
};

/*#######################
 * Renderer
 ########################*/
$renderer = new \RemoteShark\Core\MustacheRenderer();
$renderer->initialize($app);
$app['renderer'] = $renderer;
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/development.log',
));

/*#######################
 * DATABASE
 ########################*/

$app->register(new \BitolaCo\Silex\CapsuleServiceProvider(), array(
    'capsule.connection' => array(
        'driver'    => 'pgsql',
        'host'      => getenv('RS_DB_HOST'),
        'database'  => getenv('RS_DB'),
        'username'  => getenv('RS_DB_USER'),
        'password'  => getenv('RS_DB_PASSWORD'),
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    )
));
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
/*########################
 * Security
 ########################*/
/*
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
'security.firewalls' => array(
    // Login URL is open to everybody.
    'login' => array(
        'pattern' => '^/login$',
        'anonymous' => true,
    ),
    'developer' => array(
        'pattern' => '^/developer/.*$',
        'anonymous' => true,
    ),
    // Any other URL requires auth.
    'index' => array(
        'pattern' => '^.*$',
        'form'      => array(
            'login_path'         => '/login',
            'check_path'        => '/login_check',
            'username_parameter' => 'username',
            'password_parameter' => 'password',
        ),
        'anonymous' => false,
        'logout'    => array('logout_path' => '/logout'),
        'users' => array(
            // raw password is foo
            'admin' => array('ROLE_ADMIN', '5FZ2Z8QIkA7UTZ4BYkoC+GsReLf569mSKDsfods6LYQ8t+a8EW9oaircfMpmaLbPBh4FOBiiFyLfuZmTSUwzZg=='),
        ),
        // 'users'     => $app->share(function() use ($app) {
        //     return new Itaya\UserProvider($app);
        // }),
    ),
),
));*/

// Define a custom encoder for Security/Authentication
$app['security.encoder.digest'] = $app->share(function ($app) {
    // uses the password-compat encryption
    return new Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder(10);
});

$app->mount('', new RemoteShark\Core\Controller\ControllerProvider());

$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello '.$app->escape($name);
});





$app->run();
