<?php
namespace RemoteShark\Core;

interface Renderer {
	public function render($template, $data);
	public function initialize(\Silex\Application $app);
}