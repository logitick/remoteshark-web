<?php
namespace RemoteShark\Core;

class MustacheRenderer implements Renderer {
	
	private $app;

	public function render($templateFileName, $data) {
		return $this->app['mustache']->render($templateFileName, $data);
	}

	public function initialize(\Silex\Application $app) {
		$this->app = $app;	
		$app->register(new \Mustache\Silex\Provider\MustacheServiceProvider, array(
		    'mustache.path' => 'views',
		    'mustache.options' => array(
		        'cache' => 'tmp/cache/mustache',
		    ),
		));
	}
}