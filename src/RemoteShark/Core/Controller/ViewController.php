<?php
namespace RemoteShark\Core\Controller;

use Symfony\Component\HttpFoundation\Request;

abstract class ViewController implements Controller  {

	private $request;

	private $model = array();

    protected $app;

    private $view;
    public function getModel() {
      $base_url = getenv('RS_BASE_URL');
      $base_url = $base_url ? $base_url : "http://localhost/remote/";
      $this->setModelData('base_url', $base_url);
        $this->setModelData('title', $this->getTitle());
        return $this->model;
    }

    /**
     * Updates the model data if $key exists, otherwise key-value is stored.
     */
    public function setModelData($key, $value) {
        $this->model[$key] = $value;
    }

    /**
     * @return bool
     */
    public function keyExists($key) {
        return array_key_exists($key, $this->model);
    }

    /**
     * @return mixed
     */
    public function getModelData($key) {
        if (!$this->keyExists($key)) {
            return null;
        }
        return $this->model[$key];
    }

    public function getQueryStringData($key, Request $request) {
        return $request->query->get($key);
    }

    public function getPostData($key, Request $request) {
        return $request->request->get($key);
    }

    public function postDataToArray(Request $request) {
        return $request->request->all();
    }

    public function redirect($route) {
        $route = ltrim($route, '/');
        return $this->app->redirect($this->app['url_generator']->getContext()->getBaseUrl() . '/' .$route);
    }

    public abstract function getTitle();
    public function put(){}
    public function delete(){}
    public function patch(){}

    public function setApplication(\Silex\Application $app) {

        $this->app = $app;
    }

    public function createResponse($templateName) {

        return $this->app['renderer']->render($templateName, $this->getModel());
    }

}
