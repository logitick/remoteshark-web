<?php
namespace RemoteShark\Core\Controller;

class ControllerResolver extends \Silex\ControllerResolver {

    protected function instantiateController($class)
    {
    	$controller = new $class();
    	if ($controller instanceof Controller) {
    		$controller->setApplication($this->app);
    	}
        return $controller;
    }



}