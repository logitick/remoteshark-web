<?php
namespace RemoteShark\Core\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class ControllerProvider implements ControllerProviderInterface
{

    private $controllers;
    private $app;
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];
        $this->app = $app;
        $this->dispatch();
        return $this->controllers;
    }

    
    protected function dispatch() {
        $map = $this->getControllerMap();

        foreach($map as $route => $controller) {
            $interfaces = class_implements($controller);
            if (in_array('Silex\ControllerProviderInterface', $interfaces)) {
                $provider = new $controller;
                $this->controllers->mount($route, $provider->connect($this->app));
                continue;
            }
            $this->controllers->get($route, "$controller::get");
            $this->controllers->post($route, "$controller::post");
        }
    }

    protected function getControllerMap() {
        return array(
            '/' => 'RemoteShark\Controller\BaseControllerProvider',
        );
    }

}
