<?php
namespace RemoteShark\Core\Controller;

interface Controller {

	public function get();
	public function post();
	public function put();
	public function delete();
	public function patch();

	public function setApplication(\Silex\Application $app);
	public function createResponse($templateName);
}