<?php
namespace RemoteShark\Controller;

use RemoteShark\Core\Controller\ViewController;

class FaqController extends ViewController{

    public function get()
    {
        return $this->createResponse('faq');
    }

    public function post()
    {
    }

    public function getTitle()
    {
        return 'Frequently asked questions - RemoteShark';
    }
}