<?php
namespace RemoteShark\Controller\Developer;

use RemoteShark\Core\Controller\ViewController;
use RemoteShark\Entity\Developer;

class DeveloperController extends ViewController {
    public function get() {
    	$faker = \Faker\Factory::create();
		$dev = new Developer();
		$dev->setName($faker->name);
		$dev->setUsername($faker->userName);
		$dev->setPassword($faker->password);
		$this->setModelData('developer', $dev);
        return $this->createResponse('dev-info');
    }

    public function post() {

    }

    public function getTitle() {
        return 'Developer Entry';
    }
}
