<?php
namespace RemoteShark\Controller\Developer;

use RemoteShark\Core\Controller\ViewController;
use RemoteShark\Entity\Developer;

class DeveloperListController extends ViewController {

    public function get() {
        $devList = array();
        //$devList = Developer::all();
        $faker = \Faker\Factory::create();
        $default = 'media/images/image.png';
        for ($i = 0; $i < 10; $i++) {
            $dev = new Developer();
            $dev->name = $faker->name;
            $dev->imageUrl = $default;

            $dev->skillSet = array(array('skill' => $faker->domainWord), array('skill' => $faker->domainWord));

            $dev->imageUrl = $faker->imageUrl(48, 48, 'people');
            if ($i == 1) {
                $dev->lowRated = true;
            }
            $devList[] = $dev;
        }
    	$this->setModelData('developers', $devList);
        return $this->createResponse('freelancer-list');
    }

    public function post() {

    }

    public function getTitle() {
        return 'Developer Entry';
    }
}
