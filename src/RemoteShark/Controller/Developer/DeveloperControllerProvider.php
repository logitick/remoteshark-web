<?php
namespace RemoteShark\Controller\Developer;

use RemoteShark\Core\Controller\ControllerProvider;

class DeveloperControllerProvider extends ControllerProvider {

     public function getControllerMap() {
        return array(
            '/'  => 'RemoteShark\Controller\Developer\DeveloperListController',
            '/entry' => 'RemoteShark\Controller\Developer\DeveloperEntryController',
            '/{username}' => 'RemoteShark\Controller\Developer\DeveloperController',
        );
    }
}
