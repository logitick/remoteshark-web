<?php
namespace RemoteShark\Controller\Developer;

use RemoteShark\Core\Controller\ViewController;
use Symfony\Component\HttpFoundation\Request;
use RemoteShark\Entity\Developer;

class DeveloperEntryController extends ViewController {
    public function get() {
        return $this->createResponse('dev-entry');
    }

    public function post(Request $request = null) {
		$post = $this->postDataToArray($request);
		$dev = new Developer();
		$dev->username = $post['username'];
		$dev->name = $post['name'];
		$dev->password = $post['password'];
		$dev->status = 1;

		$dev->password = $this->app['security.encoder.digest']->encodePassword($post['password']);

		if ($dev->save()) {
			return $this->redirect('/developer/');
		}
        return $this->createResponse('dev-entry');
		
    }

    public function getTitle() {
        return 'Developer Entry';
    }
}
