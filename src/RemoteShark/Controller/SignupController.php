<?php
namespace RemoteShark\Controller;

use RemoteShark\Core\Controller\ViewController;

class SignupController extends ViewController{

    private $userType;

    public function get($userType = null)
    {
        $this->userType = $userType;
        return $this->createResponse('sign-up');
    }

    public function post()
    {
    }

    public function getTitle()
    {
        return 'Sign up as '. $this->userType;
    }
}
