<?php
namespace RemoteShark\Controller;

use RemoteShark\Core\Controller\ControllerProvider;

class BaseControllerProvider extends ControllerProvider {
    protected function getControllerMap() {
        return array(
            '' => 'RemoteShark\Controller\HomeController',
            'faq' => 'RemoteShark\Controller\FaqController',
            'signup/{userType}' => 'RemoteShark\Controller\SignupController',
            'login' => 'RemoteShark\Controller\LoginController',
            'marketplace' => 'RemoteShark\Controller\Developer\DeveloperControllerProvider',
            'jobs' =>  'RemoteShark\Controller\Board\JobListController',
        );
    }
}
