<?php
namespace RemoteShark\Controller;

use RemoteShark\Core\Controller\ViewController;

class LoginController extends ViewController{

    public function get()
    {
        return $this->createResponse('login');
    }

    public function post()
    {
    }

    public function getTitle()
    {
        return 'Login to RemoteShark';
    }
}
