<?php
namespace RemoteShark\Controller\Board;

use RemoteShark\Core\Controller\ViewController;
use RemoteShark\Entity\Developer;

class JobListController extends ViewController {

    public function get() {
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 10; $i++) {
            $dev = new Developer();
            $dev->name = $faker->name;
            $dev->imageUrl = '';

            $dev->skillSet = array(array('skill' => $faker->domainWord), array('skill' => $faker->domainWord));

            $dev->imageUrl = $faker->imageUrl(48, 48, 'people');
            if ($i == 1) {
                $dev->lowRated = true;
            }
            $devList[] = $dev;
        }
    	$this->setModelData('developers', $devList);
        return $this->createResponse('jobs-list');
    }

    public function post() {

    }

    public function getTitle() {
        return 'Jobs';
    }
}
