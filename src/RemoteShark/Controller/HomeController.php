<?php
namespace RemoteShark\Controller;

use RemoteShark\Core\Controller\ViewController;

class HomeController extends ViewController{

    public function get()
    {
        return $this->createResponse('home');
    }

    public function post()
    {
    }

    public function getTitle()
    {
        return 'Welcome to RemoteShark';
    }
}